#include <Eigen/Core>
#include <Eigen/Array>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdexcept>

#include <cstdlib>
#include <cstring>

#include "cmdline.h"
#include "matio.hpp"

using namespace std;
using namespace Eigen;

const string trajname = "trajectory.bin";
const string avgname = "average.bin";
const string singfile = "singular.bin";
const string leftmat = "leftmat.bin";
const string whitetraj = "white.bin";

bool verbose = false;

int main(int argc, char* argv[])
{
  cmdline::parser p;
  p.add("verbose", 'v', "For debug output");
  p.add<string>("bin", 'f', "Binary file");
  p.add<int>("dim", 'd', "No. of dimensions", false, 10);
  p.add("help", 0, "Print help");

  if (!p.parse(argc, argv) || p.exist("help")){
    cout << p.error_full() << p.usage();
    return EXIT_FAILURE;
  }

  verbose = p.exist("verbose");

  MatrixXd w;
  load(p.get<string>("bin"), w);

  for(int j = 0; j < w.cols(); ++j) {
    cout.setf(ios::scientific);
    cout << setprecision(6);
    for(int i = 0; i < p.get<int>("dim"); ++i) {
      cout << w(i, j) << " ";
    }
    cout << endl;
  }
  
  return 0;
}
