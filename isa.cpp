#include <Eigen/Core>
#include <Eigen/Array>

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdexcept>

#include <cstdlib>
#include <cstring>

#include "cmdline.h"
#include "matio.hpp"

#include "jade.h"

using namespace std;
using namespace Eigen;

const string trajname = "trajectory.bin";
const string avgname = "average.bin";
const string singfile = "singular.bin";
const string leftmat = "leftmat.bin";
const string whitetraj = "white.bin";

bool verbose = false;

int main(int argc, char* argv[])
{
  cmdline::parser p;
  p.add("verbose", 'v', "For debug output");
  p.add("three", '3', "Use 3rd order cumulant instead");
  p.add<double>("threshold", 't', "Rotation threshold", false, 1e-8);
  p.add("help", 0, "Print help");

  if (!p.parse(argc, argv) || p.exist("help")){
    cout << p.error_full() << p.usage();
    return EXIT_FAILURE;
  }

  verbose = p.exist("verbose");

  MatrixXd w;
  load(whitetraj, w);

  int D = w.rows();
  int T = w.cols();
  MatrixXd csum(D, D);
  MatrixXd rotres(D, T);
  MatrixXd rotmat(D, D);
  jade(w.data(), D, T,
       1 - (int)(p.exist("three")), p.get<double>("threshold"), (int) verbose,
       csum.data(), rotres.data(), rotmat.data());


  if(p.exist("three")){
    save("csum3.bin", csum);
    save("rotres3.bin", rotres);
    save("rotmat3.bin", rotmat);
  }else{
    save("csum.bin", csum);
    save("rotres.bin", rotres);
    save("rotmat.bin", rotmat);
  }
  
  return 0;
}
