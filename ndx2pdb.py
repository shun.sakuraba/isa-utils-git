from optparse import OptionParser
import sys

parser = OptionParser()

parser.add_option("-n", "--index", dest = "index",
                  help = "index file")

parser.add_option("-p", "--pdb", dest = "pdb",
                  help = "pdb file")
parser.add_option("-s", "--section", dest = "section",
                  help = "Target section name")

(options, args) = parser.parse_args()

ndxf = options.index

fh = open(ndxf, "rt")

line = fh.next()
m = {}

try:
    while True:
        if len(line) == 0: continue
        if line[0] == '[':
            section = (line.split())[1]
            m[section] = []
            while True:
                line = fh.next()
                if line[0] == '[':
                    break
                indexes = [int(x) for x in line.split()]
                m[section].extend(indexes)
except:
    pass

lno = 0
target = m[options.section]
inputs = []
for line in sys.stdin:
    inputs.append(float(line))

pdbf = open(options.pdb, "rt")
pdbs = []
for line in pdbf:
    if line[0:4] == "ATOM":
        pdbs.append(line)

n=0
for i in target:
    pdbs[i-1] = pdbs[i-1][0:60] + ("%6.2f\n" % inputs[n])
    n+=1

for l in pdbs:
    sys.stdout.write(l)

