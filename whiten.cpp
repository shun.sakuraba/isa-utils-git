#include <Eigen/Core>
#include <Eigen/Array>

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdexcept>

#include <cstdlib>
#include <cstring>

#include "cmdline.h"
#include "matio.hpp"

using namespace std;
using namespace Eigen;

const string trajname = "trajectory.bin";
const string avgname = "average.bin";
const string singfile = "singular.bin";
const string leftmat = "leftmat.bin";
const string whitetraj = "white.bin";

bool verbose = false;

int main(int argc, char* argv[])
{
  cmdline::parser p;
  p.add("verbose", 'v', "For debug output");
  p.add("help", 0, "Print help");
  p.add<int>("dims", 'd', "first X dimensions to take");
  
  if (!p.parse(argc, argv) || p.exist("help")){
    cout << p.error_full() << p.usage();
    return EXIT_FAILURE;
  }

  verbose = p.exist("verbose");

  MatrixXd traj;
  
  load(trajname, traj);

  int N = traj.rows();
  int T = traj.cols();
  int D = p.get<int>("dims");

  MatrixXd utmp;
  load(leftmat, utmp);
  VectorXd singtmp;
  load(singfile, singtmp);

  MatrixXd u = utmp.corner(TopLeft, N, D);
  VectorXd invsing = singtmp.start(D).cwise().inverse() * sqrt(T);

  MatrixXd r(D, T);
  r = invsing.asDiagonal() * u.transpose() * traj;

  save(whitetraj, r);
  
  return 0;
}
