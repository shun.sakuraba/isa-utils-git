#pragma once

#include <Eigen/Core>

#include <fstream>

template<typename T>
bool load(const std::string &fn, T &m)
{
  std::ifstream ifs(fn.c_str(), std::ios::binary);
  if(!ifs) return false;
  int r, c;
  ifs.read(reinterpret_cast<char*>(&r), sizeof(int));
  ifs.read(reinterpret_cast<char*>(&c), sizeof(int));
  m.resize(r, c);
  ifs.read(reinterpret_cast<char*>(m.data()), sizeof(double) * r * c);
  return true;
}

template<typename T>
void save(const std::string &fn, const T &m)
{
  std::ofstream ofs(fn.c_str(), std::ios::binary);
  int r = m.rows(), c = m.cols();
  ofs.write(reinterpret_cast<const char*>(&r), sizeof(int));
  ofs.write(reinterpret_cast<const char*>(&c), sizeof(int));
  ofs.write(reinterpret_cast<const char*>(m.data()), sizeof(double) * m.rows() * m.cols());
}
