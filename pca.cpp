#include <Eigen/Core>
#include <Eigen/Array>

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdexcept>

#include <cstdlib>
#include <cstring>

#include "cmdline.h"

extern "C" {
  /* must link libmd_d! */
#define GMX_DOUBLE
#include <statutil.h>
}

using namespace std;
using namespace Eigen;

const string trajname = "trajectory.bin";
const string avgname = "average.bin";
const string singfile = "singular.bin";
const string leftmat = "leftmat.bin";

bool verbose = false;

extern "C" void dgesvd_(const char *jobu, const char *jobvt,
                        const int *m, const int *n, 
                        double *a, const int *lda,
                        double *singval,
                        double *u, const int *ldu,
                        double *vt, const int *ldvt,
                        double *work, const int *lwork,
                        int *info);

int dgesvd_wrap(const char* jobu, const char* jobvt,
                int m, int n,
                double *a, int lda,
                double *singval,
                double *u, int ldu,
                double *vt, int ldvt)
{
  int info;
  int lwork;
  double worktmp;

  lwork = -1; 
  dgesvd_(jobu, jobvt, &m, &n, 
          a, &lda,
          singval,
          u, &ldu,
          vt, &ldvt,
          &worktmp, &lwork, &info);
  if(info != 0) return info;

  lwork = (int)worktmp;
  vector<double> work(lwork);  

  dgesvd_(jobu, jobvt, &m, &n, 
          a, &lda,
          singval,
          u, &ldu,
          vt, &ldvt,
          &work[0], &lwork, &info);

  return info;
}

extern "C" void dgesdd_(const char *jobz, 
                        const int *m, const int *n, 
                        double *a, const int *lda,
                        double *singval,
                        double *u, const int *ldu,
                        double *vt, const int *ldvt,
                        double *work, const int *lwork,
                        int *iwork,
                        int *info);

int dgesdd_wrap(const char* jobz,
                int m, int n,
                double *a, int lda,
                double *singval,
                double *u, int ldu,
                double *vt, int ldvt)
{
  int info;
  int lwork;
  double worktmp;
  vector<int> iwork(8 * min(m, n));

  lwork = -1; 
  dgesdd_(jobz, &m, &n, 
          a, &lda,
          singval,
          u, &ldu,
          vt, &ldvt,
          &worktmp, &lwork, &iwork[0], &info);
  if(info != 0) return info;

  lwork = (int)worktmp;
  vector<double> work(lwork);  

  dgesdd_(jobz, &m, &n, 
          a, &lda,
          singval,
          u, &ldu,
          vt, &ldvt,
          &work[0], &lwork, &iwork[0], &info);

  return info;
}

template<typename T>
bool load(const string &fn, T &m)
{
  ifstream ifs(fn.c_str(), ios::binary);
  if(!ifs) return false;
  int r, c;
  ifs.read(reinterpret_cast<char*>(&r), sizeof(int));
  ifs.read(reinterpret_cast<char*>(&c), sizeof(int));
  m.resize(r, c);
  ifs.read(reinterpret_cast<char*>(m.data()), sizeof(double) * r * c);
  return true;
}

template<typename T>
void save(const string &fn, const T &m)
{
  ofstream ofs(fn.c_str(), ios::binary);
  int r = m.rows(), c = m.cols();
  ofs.write(reinterpret_cast<const char*>(&r), sizeof(int));
  ofs.write(reinterpret_cast<const char*>(&c), sizeof(int));
  ofs.write(reinterpret_cast<const char*>(m.data()), sizeof(double) * m.rows() * m.cols());
}

void read_mass(const string& fn, VectorXd &masses)
{
  vector<double> mtmp;
  
  ifstream ifs(fn.c_str());
  if(!ifs) throw runtime_error("Failed to open mass file");

  double m;
  while(ifs >> m) mtmp.push_back(m);

  masses.setZero(mtmp.size());
  copy(mtmp.begin(), mtmp.end(), masses.data());
}

int main(int argc, char* argv[])
{
  cmdline::parser p;
  p.add("verbose", 'v', "For debug output");
  p.add<string>("traj", 't', "Trajectory file");
  p.add<string>("mass", 'm', "Mass file");
  p.add("help", 0, "Print help");

  if (!p.parse(argc, argv) || p.exist("help")){
    cout << p.error_full() << p.usage();
    return EXIT_FAILURE;
  }

  verbose = p.exist("verbose");

  VectorXd masses;
  read_mass(p.get<string>("mass"), masses);

  int Natom = masses.rows();
  int N = Natom * 3;
  int T = 0;

  VectorXd sqrt_masses(N);
  for(int i = 0; i < Natom; ++i){
    double sqms = sqrt(masses[i]);
    sqrt_masses(i * 3 + 0) = sqms;
    sqrt_masses(i * 3 + 1) = sqms;
    sqrt_masses(i * 3 + 2) = sqms;
  }

  string trjfile = p.get<string>("traj");

  MatrixXd traj;
  
  if(load(trajname, traj) && N == traj.rows()){
    cout << "Info: reusing existing trajectory binary file" << endl;
    T = traj.cols();
  }else{
    rvec *xs;
    int fp;
    ::real t;
    matrix box;
    int natoms;
    natoms = read_first_x(&fp, 
                          const_cast<char*>(trjfile.c_str()),
                          &t, &xs, box);

    if(natoms == 0) throw runtime_error("Error while opening trajectory");
    assert(Natom == natoms);
    
    rewind_trj(fp);

    VectorXd avg;
    VectorXd tmp;

    avg.setZero(N);
    tmp.setZero(N);

    while(true){
      int r = 
        read_next_x(fp, &t, natoms, xs, box);

      if(r == FALSE) break;
      for(int j = 0; j < natoms; ++j)
        for(int k = 0; k < 3; ++k)
          tmp[j * 3 + k] = xs[j][k];
      
      ++T;
      avg += tmp;
    }
    cerr << endl;
    rewind_trj(fp);

    avg /= (double) T;

    traj.resize(N, T);

    for(int i = 0; i < T; ++i) {
      int r = 
        read_next_x(fp, &t, natoms, xs, box);
      assert(natoms == Natom);
      assert(r == TRUE);
      for(int j = 0; j < natoms; ++j)
        for(int k = 0; k < 3; ++k)
          tmp[j * 3 + k] = xs[j][k];
  
      traj.col(i) = (tmp - avg).cwise() * sqrt_masses;
    }
    close_trj(fp);
    cerr << endl;

    // write down sqrt-mass-weighted trajectory and average
    save(trajname, traj);
    save(avgname, avg);
  }

  VectorXd singval(N);

  MatrixXd u(N, N);
  MatrixXd vt(1,1);
  
  // SVD 
  int info = dgesvd_wrap("A", "N", N, T, 
                         traj.data(), N,
                         singval.data(),
                         u.data(), N,
                         vt.data(), 1);
  if(info != 0){
    cerr << "Error: info = " << info << endl;
    exit(1);
  }

  save(singfile, singval);
  save(leftmat, u);
  
  return 0;
}
